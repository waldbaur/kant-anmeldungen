---
header:
    type: video
    src: /hintergrund.webm
    hidePromo: true
title: Festakt zum 300. Geburtstag Immanuel Kants
preview: >-
  Am 22.04.2024 findet in der Festhalle Pirmasens um 18.00 Uhr (Einlass: 17.00 Uhr) eine Festakt aus Anlass des dreihundertsten Geburtstags von Immanuel Kant statt.
  Als Fachreferent hält Professor Dr. Bernd Dörflinger (Universität Trier) einen Vortrag zum Thema „Zur Aktualität der kantischen Idee des ewigen Friedens“. Ein vielfältiges Programm zur Lebenszeit und Philosophie Kants rahmt den Vortrag ein.
date: 24.03.2024
author: Josef Traub
---


<hgroup>
<h1 style="margin-bottom: 0;">Festakt zum 300. Geburtstag Immanuel Kants</h1>
<h3>Eine gemeinsame Veranstaltung des Immanuel-Kant-Gymnasiums, des Historischen Vereins und der Stadt Pirmasens</h3>
</hgroup>

::div{.max-w-screen-lg}
![Plakat zur Veranstaltung](/300-jahre-kant/plakat.png)
::
<br><br>
Am 22.04.2024 findet in der Festhalle Pirmasens um 18.00 Uhr (Einlass: 17.00 Uhr) ein Festakt aus Anlass des dreihundertsten Geburtstags von Immanuel Kant statt.
Als Fachreferent hält Professor Dr. Bernd Dörflinger (Universität Trier) einen Vortrag zum Thema „Zur Aktualität der kantischen Idee des ewigen Friedens“. Ein vielfältiges Programm zur Lebenszeit und Philosophie Kants rahmt den Vortrag ein.
Der Eintritt zu der Veranstaltung ist frei. Karten erhalten Sie beim Immanuel-Kant-Gymnasium und beim Kulturamt.

<a href="/jetzt/300-jahre-kant/anmeldung" style="color: white;
background: #ff2db6;
text-decoration: none;
padding: 1rem 4rem;
display: block;
width: max-content;
text-align: center;
border-radius: 1rem;">Zur Anmeldung</a>
