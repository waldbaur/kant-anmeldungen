import { eq, or, and } from "drizzle-orm";
import { db } from "~/utils/db/drizzle";
import { InsertEvent, events } from "~/utils/db/schema";

export default defineEventHandler(async (ev) => {
  const { id, title, startsAt, endsAt, location, maxTickets, maxTicketsPerPerson, allowSignups, signupDeadline, hasTime } = await readBody(ev);
  if (!title || !startsAt || !location) {
    return new Response("Missing required fields", { status: 400 });
  } 
  if (typeof title !== "string" || typeof startsAt !== "string" || typeof location !== "string") {
    return new Response("Invalid title, date or location", { status: 400 });
  }
  if (maxTickets !== undefined && (typeof maxTickets !== "number" || maxTickets < 1)) {
    return new Response("Invalid number of tickets", { status: 400 });
  }
  if (maxTicketsPerPerson !== undefined && (typeof maxTicketsPerPerson !== "number" || maxTicketsPerPerson < 1)) {
    return new Response("Invalid number of tickets per person", { status: 400 });
  }
  if (allowSignups !== undefined && typeof allowSignups !== "boolean") {
    return new Response("Invalid allow signups", { status: 400 });
  }
  if (id !== undefined && typeof id !== "number") {
    return new Response("Invalid id", { status: 400 });
  }
  if (signupDeadline !== undefined && typeof signupDeadline !== "string") {
    return new Response("Invalid signup deadline", { status: 400 });
  }
  if (endsAt !== undefined && typeof endsAt !== "string") {
    return new Response("Invalid endsAt", { status: 400 });
  }
  if (hasTime !== undefined && typeof hasTime !== "boolean") {
    return new Response("Invalid hasTime", { status: 400 });
  }
  const event_details = await db
    .select()
    .from(events)
    .where(eq(events.id, id));
  if (event_details.length !== 1) {
    await db.insert(events).values({
      title,
      startsAt,
      endsAt,
      location,
      max_tickets: maxTickets,
      max_tickets_per_person: maxTicketsPerPerson,
      allow_signups: allowSignups,
      signup_deadline: signupDeadline,
      hasTime,
    });
  } else {
    const set: Partial<InsertEvent> = {};
    if (title !== undefined) set.title = title;
    if (startsAt !== undefined) set.startsAt = startsAt;
    if (endsAt !== undefined) set.endsAt = endsAt;
    if (location !== undefined) set.location = location;
    if (maxTickets !== undefined) set.max_tickets = maxTickets;
    if (maxTicketsPerPerson !== undefined) set.max_tickets_per_person = maxTicketsPerPerson;
    if (allowSignups !== undefined) set.allow_signups = allowSignups;
    if (signupDeadline !== undefined) set.signup_deadline = signupDeadline;
    if (hasTime !== undefined) set.hasTime = hasTime;
    await db.update(events).set(set).where(eq(events.id, id));
  }
  return { success: true };
});
