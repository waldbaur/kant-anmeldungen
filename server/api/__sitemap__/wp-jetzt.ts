import { asSitemapUrl, defineSitemapEventHandler } from "#imports";
import { graphql } from "~/utils/gql";
import { useQuery, provideApolloClient } from "@vue/apollo-composable";
import { ApolloClient } from "@apollo/client/core/index.js";
import { InMemoryCache } from "@apollo/client/cache/index.js";

const query = graphql(`
  query getPosts {
    posts(first: 100) {
      nodes {
        title
        date
        uri
      }
      pageInfo {
        hasNextPage
        endCursor
      }
    }
  }
`);

export default defineSitemapEventHandler(async (e) => {
  const apolloClient = new ApolloClient({
    uri: "https://wp.ikgp.de/graphql",
    cache: new InMemoryCache(),
  });

  const queryResult = provideApolloClient(apolloClient)(() => useQuery(query));
  while (queryResult.loading.value) {
    await new Promise((resolve) => setTimeout(resolve, 100));
  }
  const posts = queryResult.result.value.posts.nodes;
  return posts.map((post) => {
    return asSitemapUrl({
      loc: `/jetzt${post.uri}`,
      lastmod: post.date,
    });
  });
});
