import { db } from "~/utils/db/drizzle";
import { anmeldungen } from "~/utils/db/schema";
import { eq, and } from "drizzle-orm";

export default defineEventHandler(async (ev) => {
  const event = getRouterParam(ev, "event") as string;
  if (Number.isNaN(Number(event))) {
    return new Response("Invalid event", { status: 400 });
  }
  const { name, email, password } = await readBody(ev);
  if (!name || !email) {
    return new Response("Missing required fields", { status: 400 });
  }
  if (
    typeof password !== "string" ||
    password !== process.env.INTERN_LOGIN_PW
  ) {
    return new Response("Invalid password", { status: 403 });
  }
  if (typeof name !== "string" || typeof email !== "string") {
    return new Response("Invalid name or email", { status: 400 });
  }
  await db
    .update(anmeldungen)
    .set({
      received_at: new Date().toISOString(),
    })
    .where(
      and(
        eq(anmeldungen.name, name),
        eq(anmeldungen.email, email),
        eq(anmeldungen.event, Number(event))
      )
    );
  return { success: true };
});
