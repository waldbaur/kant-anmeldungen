import { eq, or, and } from "drizzle-orm";
import { db } from "~/utils/db/drizzle";
import { anmeldungen, events } from "~/utils/db/schema";

export default defineEventHandler(async (ev) => {
  const event = getRouterParam(ev, "event") as string;
  if (Number.isNaN(Number(event))) {
    return new Response("Invalid event", { status: 400 });
  }
  const { name, email, personen } = await readBody(ev);
  if (!name || !email || !personen) {
    return new Response("Missing required fields", { status: 400 });
  }
  if (typeof personen !== "number" || personen < 1) {
    return new Response("Invalid number of persons", { status: 400 });
  }
  if (typeof name !== "string" || typeof email !== "string") {
    return new Response("Invalid name or email", { status: 400 });
  }
  const event_details = await db
    .select()
    .from(events)
    .where(eq(events.id, Number(event)));
  if (event_details.length !== 1) {
    return new Response("Event does not exist", { status: 400 });
  }
  if (
    event_details[0].allow_signups === "false" ||
    (!event_details[0].allow_signups &&
      event_details[0]!.signup_deadline &&
      event_details[0]!.signup_deadline < new Date().toISOString())
  ) {
    return new Response("Signups are closed", { status: 400 });
  }
  if (event_details[0].max_tickets_per_person && personen > event_details[0].max_tickets_per_person) {
    return new Response("Too many tickets", { status: 400 });
  }
  const exists = await db
    .select()
    .from(anmeldungen)
    .where(
      and(
        or(eq(anmeldungen.name, name), eq(anmeldungen.email, email)),
        eq(anmeldungen.event, Number(event))
      )
    );
  if (exists.length === 1) {
    return new Response("A registration already exists.", { status: 400 });
  }
  await db.insert(anmeldungen).values({
    name,
    email,
    tickets: personen,
    event: Number(event),
  });
  return { success: true };
});
