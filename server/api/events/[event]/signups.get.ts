import { asc, desc, eq } from "drizzle-orm";
import { db } from "~/utils/db/drizzle";
import { anmeldungen } from "~/utils/db/schema";

export default defineEventHandler(async (ev) => {
  const password = await getHeader(ev, "X-Kant-Password");
  const event = getRouterParam(ev, "event") as string;
  if (Number.isNaN(Number(event))) {
    return new Response("Invalid event", { status: 400 });
  }
  if (password != process.env.INTERN_LOGIN_PW) {
    return new Response("Invalid password", { status: 403 });
  }
  return await db
    .select()
    .from(anmeldungen)
    .where(eq(anmeldungen.event, Number(event)))
    .orderBy(desc(anmeldungen.received_at), asc(anmeldungen.created_at));
});
