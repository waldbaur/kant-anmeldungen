CREATE TABLE `anmeldungen_kant` (
	`id` integer PRIMARY KEY NOT NULL,
	`name` text NOT NULL,
	`email` text NOT NULL,
	`tickets` integer NOT NULL,
	`created_at` text DEFAULT (CURRENT_TIMESTAMP),
	`received_at` text
);
--> statement-breakpoint
CREATE UNIQUE INDEX `anmeldungen_kant_email_unique` ON `anmeldungen_kant` (`email`);