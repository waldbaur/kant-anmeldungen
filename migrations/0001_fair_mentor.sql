CREATE TABLE `anmeldungen` (
	`id` integer PRIMARY KEY AUTOINCREMENT NOT NULL,
	`name` text NOT NULL,
	`email` text NOT NULL,
	`tickets` integer NOT NULL,
	`created_at` text DEFAULT (CURRENT_TIMESTAMP),
	`received_at` text,
	`event` integer NOT NULL,
	FOREIGN KEY (`event`) REFERENCES `events`(`id`) ON UPDATE no action ON DELETE no action
);
--> statement-breakpoint
CREATE TABLE `events` (
	`id` integer PRIMARY KEY AUTOINCREMENT NOT NULL,
	`title` text NOT NULL,
	`date` text NOT NULL,
	`location` text NOT NULL,
	`max_tickets` integer,
	`max_tickets_per_person` integer,
	`created_at` text DEFAULT (CURRENT_TIMESTAMP),
	`allow_signups` text,
	`signup_deadline` text
);
--> statement-breakpoint
/*
 SQLite does not support "Set autoincrement to a column" out of the box, we do not generate automatic migration for that, so it has to be done manually
 Please refer to: https://www.techonthenet.com/sqlite/tables/alter_table.php
                  https://www.sqlite.org/lang_altertable.html
                  https://stackoverflow.com/questions/2083543/modify-a-columns-type-in-sqlite3

 Due to that we don't generate migration automatically and it has to be done manually
*/--> statement-breakpoint
CREATE UNIQUE INDEX `anmeldungen_email_unique` ON `anmeldungen` (`email`);