CREATE TABLE `anmeldungen` (
	`id` integer PRIMARY KEY AUTOINCREMENT NOT NULL,
	`name` text NOT NULL,
	`email` text NOT NULL,
	`tickets` integer NOT NULL,
	`created_at` text DEFAULT (CURRENT_TIMESTAMP),
	`received_at` text,
	`event` integer NOT NULL,
	FOREIGN KEY (`event`) REFERENCES `events`(`id`) ON UPDATE no action ON DELETE cascade
);
--> statement-breakpoint
CREATE TABLE `anmeldungen_kant` (
	`id` integer PRIMARY KEY AUTOINCREMENT NOT NULL,
	`name` text NOT NULL,
	`email` text NOT NULL,
	`tickets` integer NOT NULL,
	`created_at` text DEFAULT (CURRENT_TIMESTAMP),
	`received_at` text
);
--> statement-breakpoint
CREATE TABLE `events` (
	`id` integer PRIMARY KEY AUTOINCREMENT NOT NULL,
	`title` text NOT NULL,
	`startsAt` text NOT NULL,
	`endsAt` text,
	`hasTime` text NOT NULL,
	`location` text NOT NULL,
	`max_tickets` integer,
	`max_tickets_per_person` integer,
	`created_at` text DEFAULT (CURRENT_TIMESTAMP),
	`allow_signups` text,
	`signup_deadline` text
);
--> statement-breakpoint
CREATE UNIQUE INDEX `anmeldungen_email_unique` ON `anmeldungen` (`email`);--> statement-breakpoint
CREATE UNIQUE INDEX `anmeldungen_kant_email_unique` ON `anmeldungen_kant` (`email`);