import { $ } from "bun";

process.chdir("server");
await $`bun add @libsql/linux-arm64-gnu`;
process.chdir("..");

await import("./server/index.mjs");
