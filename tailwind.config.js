/** @type {import('tailwindcss').Config} */
import {resolve} from "node:path";

export default {
    purge: {
        content: [
            resolve(__dirname, ".nuxt/content-cache/**/*.md"),
            resolve(__dirname, "content/**/*.md"),
            resolve(__dirname, "components/**/*.vue"),
            resolve(__dirname, "layouts/**/*.vue"),
            resolve(__dirname, "pages/**/*.vue"),
            resolve(
                __dirname,
                "node_modules/@nuxt/ui/dist/runtime/components/**/*.{vue,mjs,ts}"
            ),
            resolve(
                __dirname,
                "node_modules/@nuxt/ui/dist/runtime/ui.config/**/*.{vue,mjs,ts}"
            ),
        ],
    },
    safelist: ["max-w-screen-lg"],
    theme: {
        extend: {
            dropShadow: {
                "3xl": "10px 10px 7px black",
            },
            fontFamily: {
                serif: ['"Garamond Kant"', "serif"],
                coolvetica: ['"Coolvetica"', "sans-serif"],
                sans: ['"Helvetica Neue"', "sans-serif"],
                "helvetica-lt": ['"Helvetica Neue LT Std"', "sans-serif"],
            },
        },
    },
};
