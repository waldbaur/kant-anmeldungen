export type BannerConfig = {
  title: string;
  subtitle: string;
  image?: string;
  animate: boolean;
  url?: string;
  color?: string;
};
/*
export const banner: BannerConfig = {
  title: "Festakt zum 300. Geburtstag Immanuel Kants",
  subtitle:
    "Eine gemeinsame Veranstaltung des Immanuel-Kant-Gymnasiums und des Historischen Vereins Pirmasens.",
  url: "/jetzt/300-jahre-kant",
  animate: true,
  color: "#14679c",
  image: "/badge_300_jahre_kant.png",
};*/
