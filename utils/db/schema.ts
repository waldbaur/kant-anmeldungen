import { sql } from "drizzle-orm";
import { integer, sqliteTable, text } from "drizzle-orm/sqlite-core";

export const events = sqliteTable("events", {
  id: integer("id").primaryKey({ autoIncrement: true }),
  title: text("title").notNull(),
  description: text("description"),
  startsAt: text("startsAt").notNull(),
  endsAt: text("endsAt"),
  hasTime: text("hasTime").notNull(),
  location: text("location").notNull(),
  max_tickets: integer("max_tickets"),
  max_tickets_per_person: integer("max_tickets_per_person"),
  created_at: text("created_at").default(sql`(CURRENT_TIMESTAMP)`),
  allow_signups: text("allow_signups"),
  signup_deadline: text("signup_deadline"),
});

export const anmeldungen = sqliteTable("anmeldungen", {
  id: integer("id").primaryKey({ autoIncrement: true }),
  name: text("name").notNull(),
  email: text("email").unique().notNull(),
  tickets: integer("tickets").notNull(),
  created_at: text("created_at").default(sql`(CURRENT_TIMESTAMP)`),
  received_at: text("received_at"),
  event: integer("event")
    .notNull()
    .references(() => events.id, { onDelete: "cascade" }),
});

export type InsertAnmeldung = typeof anmeldungen.$inferInsert;
export type SelectAnmeldung = typeof anmeldungen.$inferSelect;
export type InsertEvent = typeof events.$inferInsert;
export type SelectEvent = typeof events.$inferSelect;
