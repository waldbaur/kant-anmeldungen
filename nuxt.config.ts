// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    "nuxt-content-assets",
    "@nuxtjs/apollo",
    "@nuxt/ui",
    "@nuxtjs/device",
    "@nuxt/content",
    "@nuxtjs/seo",
  ],
  apollo: {
    clients: {
      default: {
        httpEndpoint: "https://wp.ikgp.de/graphql",
      },
    },
    cookieAttributes: {
      // For local connections
      secure: false,
    },
  },
  ui: {
    icons: ["heroicons", "mage"],
  },
  site: {
    url: "https://ikgp.de",
    name: "Immanuel-Kant-Gymnasium Pirmasens",
    description:
      "Das Immanuel-Kant-Gymnasium Pirmasens – mehr als nur „Schule“.",
    defaultLocale: "de",
  },
  sitemap: {
    sources: ["/api/__sitemap__/content", "/api/__sitemap__/wp-jetzt", "/api/__sitemap__/wp-unsere-schule"],
    exclude: ["/jetzt/300-jahre-kant/intern"],
  },
});
