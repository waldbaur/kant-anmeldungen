FROM --platform=linux/amd64 oven/bun:1 AS build

WORKDIR /build
COPY . .

RUN apt update && apt install -y curl

RUN curl -sfLS https://install-node.vercel.app | bash -s -- 20 -y

# nuxt-content-assets causes install to hang
RUN sed -i 's/"nuxt-content-assets",/\/*"nuxt-content-assets",*\//g' nuxt.config.ts

RUN bun install

RUN sed -i 's/\/\*"nuxt-content-assets",\*\//"nuxt-content-assets",/g' nuxt.config.ts

# Build with a timeout of 5 minutes
ENV NITRO_PRESET=bun
RUN timeout 2m node node_modules/.bin/nuxi build || true

RUN rm -rf .output/server/node_modules

RUN cd .output/server && \
    timeout 1m bun install || true

FROM oven/bun:1

WORKDIR /app
COPY --from=build /build/.output .

EXPOSE 3000
COPY install.js .

CMD [ "install.js" ]
